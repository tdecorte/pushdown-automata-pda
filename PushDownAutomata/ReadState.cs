﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace PushDownAutomata
{
    // Read State overrides action to read next value by indexing into the string
    // and setting value as transitional value

    class ReadState : State
    {
        public override Stack<object> performAction(Stack<object> stack, int index, FailCheck failCheck)
        {
            string parsedString = ParsedString;

            char readLetter = parsedString[index];
            TransValue = readLetter;
            index += 1;
            Count = index;
            return stack;
        }
    }
}
