﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace PushDownAutomata
{
    class State
    {
        char m_name;
        char m_transValue;
        bool m_final;
        int m_count;
        State m_aTrans;
        State m_bTrans;
        State m_zTrans;
        static int m_pushCount = 0;
        string m_parsedString;
        
              

        public State ATrans
        {
            get { return m_aTrans; }
            set { m_aTrans = value; }
        }


        public State BTrans
        {
            get { return m_bTrans; }
            set { m_bTrans = value; }
        }


        public State ZTrans
        {
            get { return m_zTrans; }
            set { m_zTrans = value; }
        }


        public int Count
        {
            get { return m_count; }
            set { m_count = value; }
        }

        
        public bool Final
        {
            get { return m_final; }
            set { m_final = value; }
        }


        public void incPushCount()
        {
            m_pushCount++;
        }
        

        public char Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        

        public string ParsedString
        {
            get { return m_parsedString; }
            set { m_parsedString = value; }
        }
        

        public int PushCount
        {
            get { return m_pushCount; }
        }
        

        public void resetCount()
        {
            m_pushCount = 0;
        }


        public char TransValue
        {
            get { return m_transValue; }
            set { m_transValue = value; }
        }

        
        public State nextState()
        {
            char transValue = m_transValue;

            if (transValue == 'a')
                return ATrans;

            else if (transValue == 'b')
                return BTrans;

            else if (transValue == 'z')
                return ZTrans;

            else
                return null;
        }


        public virtual Stack<object> performAction(Stack<object> stack, int index, FailCheck failCheck)
        {
            return new Stack<object>();
        }
    }

}
