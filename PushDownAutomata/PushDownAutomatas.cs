﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace PushDownAutomata
{

    // Push Down Automata handles functions specific to the automata
    // Parses string, builds automata based on input and tests string on the
    // automata that was created

    class PushDownAutomatas
    {
        State m_initial;
        State m_current;
        Stack<object> m_stack;
        string m_toTest;
        int m_count = 0;
        bool m_accept = false;



        public State Initial
        {
            get
            {
                return m_initial;
            }
            set
            {
                m_initial = value;
            }
        }


        public string getParsedString()
        {
            return m_toTest;
        }




        // Tests string for the automata that was created

        public string test(string toTest)
        {
            FailCheck failCheck = new FailCheck();

            m_current = new State();
            m_current = m_initial;
            m_accept = false;
            m_toTest = toTest;
            m_toTest += 'z';
            m_count = 0;



            m_current.Count = m_count;
            m_current.resetCount();
            m_stack = new Stack<object>();


            while (m_current.Name != 'F')
            {
                m_current.ParsedString = m_toTest;
                failCheck.Fail = false;
                m_stack = m_current.performAction(m_stack, m_count, failCheck);
                m_count = m_current.Count;
                m_current = m_current.nextState();

                if (m_current == null)
                    break;

                if (failCheck.Fail == true)
                {
                    m_accept = false;
                    break;
                }

                if (m_current.Final == true)
                {
                    m_accept = true;
                    break;
                }

            }

            if (m_stack.Count == 0 && m_accept == true)
                return "accept";

            else
                return "reject";
        }



        
        
        // Loops through each transitional state in the leftmost column to 
        // determine if it has already been entered in the dictionary 
        // tempStates. If it hasn't, its respective state is added to dictionary.

        private void readStates(int index, ref string[,] tempReader,
            PushDownAutomatas pda, Dictionary<char, State> tempStates)
        {

            int numTransStates = index;
            index = 0;
            do
            {

                char stateVal = tempReader[index, 0][0];

                if (tempReader[index, 1] == "read")
                {
                    if (!tempStates.ContainsKey(stateVal))
                    {
                        State readState = new ReadState();
                        readState.Name = stateVal;
                        tempStates.Add(stateVal, readState);

                        if (index == 0)
                            pda.Initial = readState;
                    }
                }

                else if (tempReader[index, 1] == "pop")
                {
                    if (!tempStates.ContainsKey(stateVal))
                    {
                        State popState = new PopState();
                        popState.Name = stateVal;
                        tempStates.Add(stateVal, popState);
                    }
                }

                else if (tempReader[index, 1] == "push")
                {
                    if (!tempStates.ContainsKey(stateVal))
                    {
                        State pushState = new PushState();
                        pushState.Name = stateVal;
                        tempStates.Add(stateVal, pushState);
                    }
                }

                else
                {
                    throw (new PDAException("Invalid PDA action"));
                }

                index++;

            } while (index <= numTransStates);

        }


        

        
        // Loops through remaining transitional states (rightmost column)
        // and adds to dictionary if state has not yet been added.

        private void readLastStates(ref string[,] tempReader, Dictionary<char, State> tempStates)
        {
            

            int index = 0;
            char finalChar = ' ';
            do
            {
                char stateVal = tempReader[index, 3][0];
                finalChar = stateVal;

                if (!tempStates.ContainsKey(stateVal))
                {
                    State finalState = new FinalState();
                    finalState.Name = stateVal;
                    tempStates.Add(stateVal, finalState);

                    if (stateVal == 'F')
                        finalState.Final = true;
                }


                index++;

            } while (finalChar != 'F');
        }




        
        // Reads transitional letters and assigns to states

        private void readLetters(ref string[,] tempReader, Dictionary<char, State> tempStates)
        {
            int index = 0;
            
            do
            {
                char stateVal = tempReader[index, 0][0];
                char transValue = tempReader[index, 3][0];

                if (tempReader[index, 2][0] == 'a')
                {
                    tempStates[stateVal].ATrans = tempStates[transValue];
                    if (tempReader[index, 1] == "push")
                        tempStates[stateVal].TransValue = 'a';
                }

                else if (tempReader[index, 2][0] == 'b')
                {
                    tempStates[stateVal].BTrans = tempStates[transValue];
                    if (tempReader[index, 1] == "push")
                        tempStates[stateVal].TransValue = 'b';
                }

                else if (tempReader[index, 2][0] == 'z')
                {
                    tempStates[stateVal].ZTrans = tempStates[transValue];
                    if (tempReader[index, 1] == "push")
                        tempStates[stateVal].TransValue = 'z';
                }

                else
                {
                    throw (new LetterException("Invalid letter"));
                }

                index++;
            } while (tempReader[index - 1, 3][0] != 'F');
        }





        // Builds automata based on input

        public void buildAutomata(PushDownAutomatas pda, Dictionary<char, State> tempStates)
        {

            string commentLine = Console.ReadLine();
            int numState = Console.Read();
            int numStates = (int)Char.GetNumericValue(Convert.ToChar(numState));
            string line = Console.ReadLine();
            line = Console.ReadLine();

            int maxCharsInPDA = 500;
            int numValsInPDALine = 4;
            string[,] tempReader = new string[maxCharsInPDA, numValsInPDALine];
            int index = 0;
            char value;

            do
            {


                // Splits each line of PDA and copy to tempReader

                string[] parts = line.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);

                value = parts[3][0];

                tempReader[index, 0] = parts[0];
                tempReader[index, 1] = parts[1];
                tempReader[index, 2] = parts[2];
                tempReader[index, 3] = parts[3];




                // If final state, no more states of PDA to read

                if (value == 'F')
                    break;

                line = Console.ReadLine();
                index++;

            } while (value != 'F');




            // Reads states

            readStates(index, ref tempReader, pda, tempStates);
            readLastStates(ref tempReader, tempStates);



            // Reads transitional letters

            readLetters(ref tempReader, tempStates);


        }

    }
}
