﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace PushDownAutomata
{
    // Push State overrides action to push transitional value onto stack

    class PushState : State
    {
        public override Stack<object> performAction(Stack<object> stack, int index, FailCheck failCheck)
        {
            int pushCount = PushCount;
            if (pushCount == 0)
                stack.Push('z');

            char transValue = TransValue;
            stack.Push(transValue);

            Count = index;
            incPushCount();
            return stack;
        }
    }
}
