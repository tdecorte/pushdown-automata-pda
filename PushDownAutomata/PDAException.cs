﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace PushDownAutomata
{
    public class PDAException : ApplicationException
    {
        public PDAException(string message) : base (message)
        {
        }
    }


    public class LetterException : PDAException
    {
        public LetterException(string message)
            : base(message)
        {
        }
    }
}
