﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace PushDownAutomata
{
    // Fail Check tracks fail States

    class FailCheck
    {
        bool m_fail = false;

        public bool Fail
        {
            get
            {
                return m_fail;
            }
            set
            {
                m_fail = value;
            }
        }

    }
}
