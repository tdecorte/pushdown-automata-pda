﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace PushDownAutomata
{

    // Pop State overrides action to pop transitional value from stack
    // Handles empty stack by pushing z to stack

    class PopState : State
    {
        public override Stack<object> performAction(Stack<object> stack, int index, FailCheck failCheck)
        {
            bool fail = true;


            if (stack.Count == 0)
            {
                stack.Push('z');
                TransValue = 'z';
            }


            string topStack = stack.Peek().ToString();
            char topVal = topStack[0];
            Count = index;

            if (ATrans != null && topVal == 'a')
                TransValue = 'a';

            if (BTrans != null && topVal == 'b')
                TransValue = 'b';

            if (ZTrans != null && topVal == 'z')
                TransValue = 'z';

            if (TransValue != topVal)
                failCheck.Fail = fail;

            stack.Pop();
            return stack;
        }
    }

}
