﻿// Tracey DeCorte
// CST229 - Assignment 3.2
// Feb. 20, 2013
// Push-down automata

/*
 * Program takes input following the format below
 * in order to determine whether string is valid:
 * 
 * 'no. a's = no. b's
 * 8
 * S, read, a, A
 * S, read, b, B
 * S, read, z, C
 * A, pop, a, X
 * A, pop, b, S
 * A, pop, z, Y
 * X, push, a, Y
 * Y, push, a, S
 * B, pop, a, S
 * B, pop, b, U
 * B, pop, z, V
 * U, push, b, V
 * V, push, b, S
 * C, pop, z, F
 * ab
 * aaabb
 * ababab
 * aaa
 * bbbaaa
 * aaaabbbb
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;



namespace PushDownAutomata
{


    class Program
    {

        static void Main(string[] args)
        {
            PushDownAutomatas pda = new PushDownAutomatas();
            Dictionary<char, State> tempStates = new Dictionary<char, State>();
            string tempLine = "";

            try
            {
                pda.buildAutomata(pda, tempStates);
            }
            catch (PDAException e)
            {
                Console.WriteLine("{0}: {1}", e.GetType(), e.Message);
            }




            string tempChar = "";
            bool check = true;

            do
            {
                tempChar = "";
                tempLine = "";


                // Read in string as characters and append as string

                while (tempChar != Environment.NewLine)
                {

                    ConsoleKeyInfo holder;

                    if (!Console.KeyAvailable)
                    {
                        check = false;
                        break;
                    }


                    holder = Console.ReadKey(true);
                    tempChar = holder.KeyChar.ToString();

                    if (tempChar == "\r")
                        break;

                    tempLine += tempChar;
                }



                // Test string and input results
                
                string result = pda.test(tempLine);

                Console.Out.NewLine = "    ";
                Console.WriteLine(result);
                Console.Out.NewLine = Environment.NewLine;
                Console.WriteLine(tempLine);
                


                // If no more strings to be read, stop reading

                if (check == false)
                    break;


            } while (check == true);

            Console.ReadLine();
        }

    }
}
